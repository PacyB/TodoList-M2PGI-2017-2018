UE DEVELOPPEMENT MOBILE M2GI 2017/2018

Membres : Faye Paul & Tchamdja Yaovi

						Application : TodoList 
1- CONSIGNES DE COMPILATION:
	+Pour tester l'application sur votre navigateur utiliser : ionic serve [options]
		avec Options  = -l (mode lab ou live-reload pour recharger apres chaque modification du code) -c et -s pour les 		log de la console et du serveur respectivement.
 	+Pour faire un test sur un device il faut au préalable : 
		1- Ajouter la plateforme du device testé : ionic cordova platform add [ios|android]
		2- puis lancer l application sur le device : ionic cordova run [android|ios]
	+Pour generer un APK de l application il faudra faire un build : ionic cordova build [android|ios] --prod

2- Détails / Specificités de l application : 
	L application TodoList permet à ses utilisateurs de s'authentifier (et ainsi d avoir un espace personnel) pour gerer leur listes de taches à faire. Elle comporte en cela une page Liste avec diversites fonctions permettant sa gestion. Cette page liste est associées à un ensemble de taches(items) qui seront repertoriées dans une page egalement avec les fonctionnalités qui vont bien.

3- Détails des fonctionnalités réalisées :
	Nous présentenrons les differentes fonctionnalités implémentées chronologiquement en fonction de leur page d appartenance :

	3.1 Page d'Acceuil (Home)
		Au niveau de la page home de l application il nous est possbile : 
+ DE S'INSCRIRE : pratique si l utilisateur courant ne possedait pas de compte au prealable et il peut y acceder en appuyant sur le bouton "Creer un nouveau compte". Un clic sur ce dernier nous menera à une page d inscription de type formulaire où l utilisateur pourra saisir son nom,prenom,email,mot de passe et selectionner une date de naissance et un sexe. Une fois les champs saisis, il peut valider pour etre rediriger à la page d acceuil 
+ DE SE CONNECTER : en rentrant un email et un mot de passe valide puis acceder à la page Liste.
	°Additionel : une fonction permet egalement à l utilisateur de recevoir un message adéquat (ToastController) si sa saisie est erronée
+ DE SE CONNECTER AVEC UN COMPTE GOOGLE : en appuyant sur le bouton affrété et en choisissant un compte google à utiliser. la gestion de la premiere connexion (qui fait office d inscription au niveau de la databse) et des autres connexions est faite. Malheureusement seul la partie Web a été testée (la partie native a été implementée mais pas testée faute de plugin (code en commentaire))

	3.2 Page des Listes (Todo-service)
		A ce stade, si l utilisateur avait deja des données crees auparavant, il aura en affichage l ensemble des listes qu il a crée sinon seul une liste par defaut s affichera. Les fonctions possibles dans cette page sont :

<Bouton Header>
+ AJOUTER UNE NOUVELLE LISTE : en cliquant sur le bouton "add" au niveau du header, une fenetre (alertController) est generée pour permettre la saisie du nom de la liste à creer
+ SE DECONNECTER : pour finir sa session et revenir a la page Home (bouton power)
+ VOIR SA PAGE DE PROFIL : en cliquant sur le bouton "person" et ainsi voir l ensemble des données saisie lors de l inscription associées à ce compte

<Bouton Sliding vers la gauche>
+ DE MODIFIER : la liste typiquement le nom, encore une fois à l aide d une fenetre (Alertcontroller) (slide vers la gauche)
+ DE RAJOUTER/ASSOCIER UNE PHOTO : à une liste selectionnée. 
NB : Dans l ideal il aurait fallu donner la possibilité à l utilisateur de choisir sa photo avec les plugins natifs (FileChooser et File) ou de prendre une photo depuis sa camera (avec plugin Camera de cordova) mais dans notre cas les photos prises sont stockées au niveau du storage de firebase et sont choisis aleatoirement pour etre chargée et associée à la liste selectionnée. Ceci du au fait que j arrivais pas à faire tourner les dits-plugins et faire des test dessus.
+ DE SUPPRIMER : la liste selectionnée avec une fenetre (alertController) de confirmation qui redemandera si l action doit etre faite.

	3.3 Page des Taches (todo-items)
		Dans cette page on peut noter que le titre porte bien le nom de la liste dans lequel on est et les fonctions possibles sont :

<Bouton Header>
+AJOUTER UNE NOUVELLE TACHE : qui va generer une page (modal) avec 2 champs en entree la description et le nom de la tache (on suppose qu une nouvelle tache crée possede un etat non fini i.e incomplet) puis une validation qui va nous ramener sur la page de tache
+REVENIR A LA PAGE PRECEDENTE : à savoir la page des listes   

<Bouton Slide Gauche et Droite>
+MODIFIER UNE TACHE : en appuyant sur un bouton modifier (slide vers la gauche) qui nous generera une page(modal) avec 2 champs egalement pour changer la description et le nom
+SUPPRIMER UNE TACHE : en appuyant sur le bouton "trash"(slide vers gauche) qui affichera une fenetre (alert ) de confirmation avant execution
+FINIR UNE TACHE : en appuyant sur le bouton valider (slide vers la droite) qui permet de passer l etat d une tache de "pas fini" à "fini". A noter que lorsque le bouton est appuyé sur une tache deja finie une fenetre pop pup apparait pour signaler que la tache est deja terminée

4- Global Review : 
	Dans son integralité, l application est stockée sur firebase Database avec un espace alloué à chaque nouvel utilisateur pour stocker ses informations personnelles et ses listes et taches assciées. Le stockage des medias (images) est fait sur Storage
PS : j avais egalement implémenté le partage des listes avec l envoi d' un mail contenant l URL de la liste à partager(bouton share) à un utilisateur(U2). A la reception U2 ira dans une page shared list, appuyera sur un bouton d ajout et rentrera le l url recu pour faire le partage (en temps reel de la liste). Malheuresement!! mon code est trop instable en l état actuel pour que je me permette de le pusher sur le git. je pourrai tourjours le pusher si vous voulez vous contenter du code.
      
		
