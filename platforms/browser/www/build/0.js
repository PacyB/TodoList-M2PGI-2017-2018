webpackJsonp([0],{

/***/ 813:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TodoAddItemPageModule", function() { return TodoAddItemPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__todo_add_item__ = __webpack_require__(816);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TodoAddItemPageModule = (function () {
    function TodoAddItemPageModule() {
    }
    TodoAddItemPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__todo_add_item__["a" /* TodoAddItemPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__todo_add_item__["a" /* TodoAddItemPage */]),
            ],
        })
    ], TodoAddItemPageModule);
    return TodoAddItemPageModule;
}());

//# sourceMappingURL=todo-add-item.module.js.map

/***/ }),

/***/ 816:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TodoAddItemPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(38);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TodoAddItemPage = (function () {
    function TodoAddItemPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.task = "";
        this.desc = "";
    }
    TodoAddItemPage.prototype.backToItems = function () {
        var info = {
            TaskName: "",
            Description: ""
        };
        this.viewCtrl.dismiss(info);
    };
    TodoAddItemPage.prototype.validData = function () {
        console.log("Log check value Task: " + JSON.stringify(this.task));
        console.log("Log check value Desc: " + JSON.stringify(this.desc));
        var info = {
            TaskName: this.task,
            Description: this.desc
        };
        this.viewCtrl.dismiss(info);
    };
    TodoAddItemPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TodoAddItemPage');
    };
    TodoAddItemPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-todo-add-item',template:/*ion-inline-start:"/home/hacknblast/Bureau/Ab New School/FOURTH YEAR/S2/Développement Mobile/TPS/TodoList_v0/src/pages/todo-add-item/todo-add-item.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Tâche </ion-title>\n    <ion-buttons start>\n      <button ion-button icon-only (click)="backToItems()" color="Greycol">\n        <ion-icon name="arrow-dropleft-circle"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-list>\n    <ion-item>\n      <ion-label color="primary" floating>Tâche</ion-label>\n      <ion-input type="text" [(ngModel)]="task"></ion-input>\n    </ion-item>\n  \n    <ion-item>\n      <ion-label color="primary" floating>Description</ion-label>\n      <ion-input type="text" [(ngModel)]="desc"></ion-input>\n    </ion-item>\n    \n    <div class="row" >\n      <div class="col" ></div>\n      <div class="col" >\n        <button ion-button item-end color="secondary" (click)="validData()">\n          <ion-icon name="checkmark-circle"> Valider</ion-icon>\n        </button>\n      </div>\n      <div class="col" ></div>\n    </div>\n    \n\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/home/hacknblast/Bureau/Ab New School/FOURTH YEAR/S2/Développement Mobile/TPS/TodoList_v0/src/pages/todo-add-item/todo-add-item.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ViewController */]])
    ], TodoAddItemPage);
    return TodoAddItemPage;
}());

//# sourceMappingURL=todo-add-item.js.map

/***/ })

});
//# sourceMappingURL=0.js.map