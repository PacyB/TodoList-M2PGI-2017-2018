export interface TodoList {
    name : string,
    uuid : string
  }
  
  export interface TodoItem {
    lid? : string,
    name : string,
    desc? : string,
    complete : boolean
  }

  export interface User {
    email : string,
    password : string,
    firstName : string,
    lastName : string,
    birthday : string,
    sex : string 
  }