import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { TodoServicePage } from '../todo-service/todo-service';
import { InscriptionPage } from '../inscription/inscription';
import { AngularFireAuth } from 'angularfire2/auth';
import { GooglePlus } from '@ionic-native/google-plus';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase/app';
import { Platform } from 'ionic-angular';
import {Observable} from "rxjs/Observable";
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnDestroy, OnInit {
  private ngUnsubscribe: Subject<boolean> = new Subject();
  user: Observable<firebase.User>;
  subscription:any;
  constructor(public navCtrl: NavController,  public authCtrl:AngularFireAuth, public fireDB: AngularFireDatabase,
              public msg:ToastController, public google:GooglePlus, public platf:Platform) {
       
         this.user = this.authCtrl.authState;       
  }

  async showTodoPage(login:string,mdp:string) {
    try {
      const result  = await this.authCtrl.auth.signInWithEmailAndPassword(login,mdp);  
      if(result){
        this.authCtrl.auth.onAuthStateChanged((user) => {
          if (user) {
            this.navCtrl.push(TodoServicePage,{
              arg1: user
            });
          }
        });
        
      }
    }catch (error) {
        this.msg.create({
          message: error,
          duration: 5000
        }).present();
    }
    
    
  }
  
  public createUser() {
    this.navCtrl.push(InscriptionPage);
  }

  loginGoogle(){
    /*if(this.platf.is('cordova')){ 
      this.nativeLogin();
    }else{*/
      this.webLogin();
   // }  
  }

  async nativeLogin(): Promise<void>{
    try {
      const googleUser = await this.google.login({
        'webClientId' : '229004801619-dc3bdboe3lg9j59k1i934u31qo4fac16.apps.googleusercontent.com',
        'offline' : true,
        'scopes': 'profile email'
      });
      return await this.authCtrl.auth.signInWithCredential(
       // Vrai -> //firebase.auth.GoogleAuthProvider.credential(googleUser.idToken)
        googleUser.idToken
      );

    } catch (error) {
      console.log(error);
    }
  }

  async webLogin(): Promise<void>{
    try {
      const provider = new firebase.auth.GoogleAuthProvider();
      provider.setCustomParameters({
        prompt:'select_account'
      });
      const credential = await this.authCtrl.auth.signInWithPopup(provider);
      if(credential){
        this.authCtrl.auth.onAuthStateChanged((user) => {
          if (user) {

            this.subscription =  this.fireDB.object('/Users/')
            .snapshotChanges().map(action => {
              const data = action.payload.toJSON();
              return data;
            }).takeUntil(this.ngUnsubscribe)
            .subscribe(result => {
              let returnedArray = [];
              Object.keys(result).map(key => {
                returnedArray.push({ 'key': key, 'data':result[key]});
              }); 
              let affined = returnedArray.map(val => val.key);
              let check  = false;
              while(affined.length > 0){
                let current_id = affined.pop();
                if(current_id === user.uid){
                  check = true;
                }
              }
              console.log("Utilisateur existant ? : "+check); 
              
              if(check){
                this.ngOnDestroy()
               console.log(" Compte deja existant -> Connexion normale");
               this.navCtrl.push(TodoServicePage,{
                 arg1: user
               });
              }else{
                console.log(" Nouveau compte -> Inscription + connexion");
                let userbis ={ 
                  firstName : user.displayName,
                  lastName : "Unknown",
                  birthday : "Unknown",
                  sex : "Unknown"             
                };
                this.fireDB.list('/Users').set(user.uid, userbis);
                this.fireDB.list('/Users/'+user.uid+'/Lists').push({
                    uuid : "0",
                    name : "Default"
                });
                this.fireDB.list('/Users/'+user.uid+'/Items').push({
                  lid: "0",
                  name: "Default",
                  desc: "",
                  complete: "false"
                });
                this.ngOnDestroy()
                this.navCtrl.push(TodoServicePage,{
                  arg1: user
                });
              }
            });
          }
        }); 
      }
    
    } catch (error) {
      
    }
  }

  ngOnInit(){

  }
  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  logoutGoogle(){
    this.authCtrl.auth.signOut();
    /*if(this.platf.is('cordova')){
      this.google.logout();
    }*/
  }

}
