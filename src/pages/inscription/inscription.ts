import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { User } from "../../model/model";
import { Injectable } from '@angular/core';

@Injectable()
@IonicPage()
@Component({
  selector: 'page-inscription',
  templateUrl: 'inscription.html',
})
export class InscriptionPage {
user = {} as User;
  constructor(public navCtrl: NavController, public navParams: NavParams,
          public authCtrl:AngularFireAuth, public fireDB:AngularFireDatabase) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InscriptionPage');
  }

  async subscribe(userprof: User){
    try {
      const result = await this.authCtrl.auth.createUserWithEmailAndPassword(userprof.email,userprof.password);
      console.log("Inscription Valide!");
      if(result){
        this.authCtrl.auth.onAuthStateChanged((user) => {
          if (user) {
            let userbis ={ 
              firstName : userprof.firstName,
              lastName : userprof.lastName,
              birthday : userprof.birthday,
              sex : userprof.sex             
            };
            this.fireDB.list('/Users').set(user.uid, userbis);
            this.fireDB.list('/Users/'+user.uid+'/Lists').push({
                uuid : "0",
                name : "Default"
            });
            this.fireDB.list('/Users/'+user.uid+'/Items').push({
              lid: "0",
              name: "Default",
              desc: "",
              complete: "false"
          });
          }
        });
        
      }
      this.navCtrl.pop();
    } catch (error) {
      console.log("Erreur Inscription "+error);
    }
   
  }

}
