import { Component, OnDestroy, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';
@IonicPage()
@Component({
  selector: 'page-profil',
  templateUrl: 'profil.html',
})
export class ProfilPage implements OnDestroy,OnInit {
  private ngUnsubscribe: Subject<boolean> = new Subject();
  profilDB = {
    firstName : "",
    lastName : "",
    birthday : "",
    sex : ""
  };
  subscription : any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
            public A_database: AngularFireDatabase) {
            console.log ('Connected as '+this.navParams.get('user').email);

    
  }

  ngOnInit(){
    this.subscription = this.A_database.object('/Users/'+this.navParams.get('user').uid)
    .snapshotChanges().map(action => {
      const data = action.payload.toJSON();
      return data;
    }).takeUntil(this.ngUnsubscribe)
    .subscribe(result => {
      let Valretour = [];
      Object.keys(result).map(key => {
        Valretour.push({ 'key': key, 'data':result[key]});
      }); 
      this.profilDB.birthday = Valretour[2].data;
      this.profilDB.firstName = Valretour[3].data;
      this.profilDB.lastName = Valretour[4].data;
      this.profilDB.sex = Valretour[5].data;
    })
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilPage');
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
