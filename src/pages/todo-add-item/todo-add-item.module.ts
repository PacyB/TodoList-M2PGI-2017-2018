import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TodoAddItemPage } from './todo-add-item';

@NgModule({
  declarations: [
    TodoAddItemPage,
  ],
  imports: [
    IonicPageModule.forChild(TodoAddItemPage),
  ],
})
export class TodoAddItemPageModule {}
