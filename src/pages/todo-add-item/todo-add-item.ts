import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-todo-add-item',
  templateUrl: 'todo-add-item.html',
})
export class TodoAddItemPage {
task:String;
desc:String;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.task = "";
    this.desc = "";
  }

  public backToItems(){
    let info ={
      TaskName : "",
      Description: ""
    };
    this.viewCtrl.dismiss(info);

  }

  public validData(){
    console.log("Log check value Task: "+JSON.stringify(this.task));
    console.log("Log check value Desc: "+JSON.stringify(this.desc));
    let info ={
      TaskName : this.task,
      Description: this.desc
    };
    this.viewCtrl.dismiss(info);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TodoAddItemPage');
  }

}
