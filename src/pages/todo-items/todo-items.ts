import { Component, OnDestroy, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import {TodoItem} from "../../model/model";
import {Observable} from "rxjs/Observable";
import 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

@Injectable()
@IonicPage()
@Component({
  selector: 'page-todo-items',
  templateUrl: 'todo-items.html',
})
export class TodoItemsPage implements OnDestroy, OnInit {
private ngUnsubscribe: Subject<boolean> = new Subject();
items:TodoItem[] = [];
title:String;
pathItems : any;
subscription: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public alertCtrl: AlertController, public modalCtrl : ModalController,
      private A_database: AngularFireDatabase ) {
    
    this.title = this.navParams.get('selectedList').name; 
    this.pathItems = 'Users/'+this.navParams.get('user').uid+'/Items/';
                      
    //console.log("Path Items : "+this.pathItems);
  }

  backToList(){
    this.navCtrl.pop();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad TodoItemsPage');
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
  ngOnInit() {

   this.subscription = this.A_database.object(this.pathItems)
    .snapshotChanges().map(action => {
     const data = action.payload.toJSON();
        return data;
     }).takeUntil(this.ngUnsubscribe)
     .subscribe(result => {
        let returnedArray = [];
        Object.keys(result).map(key => {
          returnedArray.push({ 'key': key, 'data':result[key]});
        }); 
        let dataStore = returnedArray.map(val => val.data);
        let affined = [];
        dataStore.map(val =>{
            if(val.lid == this.navParams.get('key')){
              affined.push(val);
            }
        });

      if(affined.length > 0){
        //console.log(affined);
        this.getTodo(affined).takeUntil(this.ngUnsubscribe).subscribe(result => this.items = result);
      }                               
     });
  }

  public addItem(){  
    let modal = this.modalCtrl.create('TodoAddItemPage');
      modal.onDidDismiss((info) => {
        if(info.TaskName != ""){
          let addedItem = {
            lid: this.navParams.get('key'),
            name: info.TaskName,
            desc: info.Description,
            complete: false
          }
          this.A_database.list(this.pathItems).push(addedItem);
        }
      })
      modal.present();
  }

  public deleteItemConfirm(selectedItem:TodoItem) {
    let confirm = this.alertCtrl.create({
      title: 'Suppression',
      message: 'Etes vous sûr de vouloir supprimer cette tâche ?',
      buttons: [
        {
          text: 'Non',
          handler: () => {
            console.log('Non clicked');
          }
        },
        {
          text: 'Oui',
          handler: () => {
            console.log('Oui clicked');
            console.log("Log check value Item: "+JSON.stringify(selectedItem.name));
            let deletedItemIndex = this.items.findIndex(d => d.name == selectedItem.name);  
           this.subscription = this.A_database.object(this.pathItems)
            .snapshotChanges().map(action => {
             const data = action.payload.toJSON();
                return data;
             }).takeUntil(this.ngUnsubscribe)
             .subscribe(result => {
                let returnedArray = [];
                Object.keys(result).map(key => {
                  returnedArray.push({ 'key': key, 'data':result[key]});
                }); 
                
                let affined = [];
                returnedArray.map(val =>{
                    if(val.data.lid == this.navParams.get('key')){
                      affined.push(val);
                    }
                });
              
              try {
                if(affined.length > 0){
                  //console.log(affined);
                  this.A_database.list(this.pathItems).remove(affined[deletedItemIndex].key);
                } 
              } catch (error) {
                console.log('undefined error');
              }         
            });
          }
        }
      ]
    });
    confirm.present();
  }

  finishTask(item:TodoItem){
      if(item.complete != true){
        var taskItemIndex = this.items.findIndex(d => d.name == item.name);
        
      this.subscription =  this.A_database.object(this.pathItems)
        .snapshotChanges().map(action => {
         const data = action.payload.toJSON();
            return data;
         }).takeUntil(this.ngUnsubscribe)
         .subscribe(result => {
            let returnedArray = [];
            Object.keys(result).map(key => {
              returnedArray.push({ 'key': key, 'data':result[key]});
            }); 
            
            let affined = [];
            returnedArray.map(val =>{
                if(val.data.lid == this.navParams.get('key')){
                  affined.push(val);
                }
            });
          
          try {
            if(affined.length > 0){
              //console.log(affined);
              affined[taskItemIndex].data.complete = true;
              this.A_database.list(this.pathItems).update(affined[taskItemIndex].key,affined[taskItemIndex].data);
            } 
          } catch (error) {
            console.log('error');
          }         
        });
      }else{
        let alert = this.alertCtrl.create({
          title: 'Tâche',
          message: 'Cette tâche a déja été terminée!',
          buttons: ['OK']
        });
        alert.present();
      }
  }
  public modifItem(item:TodoItem){
    let modal = this.modalCtrl.create('TodoAddItemPage');
    modal.onDidDismiss((info) => {
      if(info.TaskName != ""){
        let selectedItem = {
          lid: item.lid,
          name: info.TaskName,
          desc: info.Description,
          complete: item.complete
        }
        var modItemIndex = this.items.findIndex(d => d.name == item.name);
        this.subscription = this.A_database.object(this.pathItems)
        .snapshotChanges().map(action => {
         const data = action.payload.toJSON();
            return data;
         }).takeUntil(this.ngUnsubscribe)
         .subscribe(result => {
            let returnedArray = [];
            Object.keys(result).map(key => {
              returnedArray.push({ 'key': key, 'data':result[key]});
            }); 
            
            let affined = [];
            returnedArray.map(val =>{
                if(val.data.lid == this.navParams.get('key')){
                  affined.push(val);
                }
            });
          
          try {
            if(affined.length > 0){
              //console.log(affined);
              this.A_database.list(this.pathItems).update(affined[modItemIndex].key,selectedItem);
            } 
          } catch (error) {
            console.log('error');
          }         
        });
      }
    })
    modal.present();
  }

  public getTodo(data:TodoItem[]): Observable<TodoItem[]> {
    return Observable.of(data);
  }


}
