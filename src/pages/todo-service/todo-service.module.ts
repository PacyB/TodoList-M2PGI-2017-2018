import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TodoServicePage } from './todo-service';

@NgModule({
  declarations: [
    TodoServicePage,
  ],
  imports: [
    IonicPageModule.forChild(TodoServicePage),
  ],
})
export class TodoServicePageModule {}
