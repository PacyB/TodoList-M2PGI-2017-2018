import { Component, OnDestroy, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { TodoList } from "../../model/model";
import{ TodoItemsPage }from '../todo-items/todo-items';
import{ ProfilPage }from '../profil/profil';
import {Observable} from "rxjs/Observable";
import { storage } from 'firebase';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

@Injectable()
@IonicPage()
@Component({
  selector: 'page-todo-service',
  templateUrl: 'todo-service.html',
})
export class TodoServicePage implements OnDestroy, OnInit {
  private ngUnsubscribe: Subject<boolean> = new Subject();
  lists : TodoList[] = [];
  pathList : any; 
  subscription: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController,
              private A_database: AngularFireDatabase, private authCtrl: AngularFireAuth) {
          console.log("Current user UID : "+ this.navParams.get('arg1').uid);
          this.pathList = 'Users/'+this.navParams.get('arg1').uid+'/Lists/';       
          console.log("Path List : " + this.pathList);     
  }

  ngOnInit(){ 
    this.subscription =  this.A_database.object(this.pathList)
      .snapshotChanges().map(action => {
        const data = action.payload.toJSON();
        return data;
      }).takeUntil(this.ngUnsubscribe)
      .subscribe(result => {
        let returnedArray = [];
        Object.keys(result).map(key => {
          returnedArray.push({ 'key': key, 'data':result[key]});
        }); 
        let affined = returnedArray.map(val => val.data);
        this.getList(affined).takeUntil(this.ngUnsubscribe).subscribe(lists => this.lists = lists);
        //console.log(this.lists);
      })
  }
  
  ngOnDestroy(){
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  
  ionViewDidLoad() {
    console.log('ionViewDidLoad TodoServicePage');
       
  }

 async listSelected(item:TodoList){
    let returnedArray = [];
    var ItemIndex = this.lists.findIndex(d => d.name == item.name);
  this.subscription = await this.A_database.object(this.pathList)
    .snapshotChanges().map(action => {
      const data = action.payload.toJSON();
      return data;
    }).takeUntil(this.ngUnsubscribe)
    .subscribe(result => {
      Object.keys(result).map(key => {
        returnedArray.push({ 'key': key, 'data':result[key]});
      });
    
      this.navCtrl.push(TodoItemsPage,{
        selectedList : item,
        key : returnedArray[ItemIndex].key,
        user : this.navParams.get('arg1')
      });

    });
    
  }

  async logOut(){
    try {
      await this.authCtrl.auth.signOut().then(rej =>{
          this.A_database.database.ref(this.pathList).onDisconnect().cancel();        
          this.pathList = "";
          this.navCtrl.pop();
          console.log('Disconnected successfully!');
      });       
    }catch (error) {
        console.log("Probleme de deconnexion");
    }
    
  }


  public addList(){  
    let prompt = this.alertCtrl.create({
        title: 'Nouvelle Liste',
        message: "Veuillez saisir le nom de la liste à ajouter",
        inputs: [
          {
            name: 'newList',
            placeholder: 'Nom'
          },
        ],
        buttons: [
          {
            text: 'Annuler',
            handler: data => {
              console.log('Annuler clicked');
            }
          },
          {
            text: 'Confirmer',
            handler: data => { 
              this.A_database.list(this.pathList).push({
                uuid: this.navParams.get('arg1').uid,
                name: data.newList
              });
            }
          }
        ]
      });
      prompt.present();
  }

  public deleteListConfirm(selectedList:TodoList) {
    let confirm = this.alertCtrl.create({
      title: 'Suppression',
      message: 'Etes vous sûr de vouloir supprimer cette liste ?',
      buttons: [
        {
          text: 'Non',
          handler: () => {
            console.log('Non clicked');
          }
        },
        {
          text: 'Oui',
          handler: () => {
            console.log('Oui clicked');
            console.log("Log check value Item: "+JSON.stringify(selectedList.name));
            let returnedArray = [];
            var deletedItemIndex = this.lists.findIndex(d => d.name == selectedList.name);
            this.subscription = this.A_database.object(this.pathList)
            .snapshotChanges().map(action => {
              const data = action.payload.toJSON();
              return data;
            }).takeUntil(this.ngUnsubscribe)
            .subscribe(result => {
              Object.keys(result).map(key => {
                returnedArray.push({ 'key': key, 'data':result[key]});
              });
            //console.log(returnedArray[deletedItemIndex].key);
            this.A_database.list(this.pathList).remove(returnedArray[deletedItemIndex].key);
            });
          }
        }
      ]
    });
    confirm.present();
  }
  public modifList(item:TodoList){
    let prompt = this.alertCtrl.create({
      title: 'Renommer Liste',
      message: "Veuillez saisir le nouveau nom de la liste",
      inputs: [
        {
          name: 'modList',
          placeholder: 'Nom'
        },
      ],
      buttons: [
        {
          text: 'Annuler',
          handler: data => {
            console.log('Annuler clicked');
          }
        },
        {
          text: 'Confirmer',
          handler: data => {
            let returnedArray = [];
            var modItemIndex = this.lists.findIndex(d => d.name == item.name);
            var UpdatedList = {
                name: data.modList,
                uuid: this.lists[modItemIndex].uuid
            }
           this.subscription = this.A_database.object(this.pathList)
            .snapshotChanges().map(action => {
              const data = action.payload.toJSON();
              return data;
            }).takeUntil(this.ngUnsubscribe)
            .subscribe(result => {
              Object.keys(result).map(key => {
                returnedArray.push({ 'key': key, 'data':result[key]});
              });
             
            this.A_database.list(this.pathList).update(returnedArray[modItemIndex].key,UpdatedList);
            });
          }
        }
      ]
    });
    prompt.present();
  }

  showProfil(){
    this.navCtrl.push(ProfilPage,{
      user : this.navParams.get('arg1')
    });
  }
  public getList(data:TodoList[]): Observable<TodoList[]> {
    return Observable.of(data);
  }

  async selectPhoto(item: TodoList){
    try{
      let randomVal = Math.floor(Math.random() * 10) + 1 ;
      let returnedArray = [];
      var ItemIndex = this.lists.findIndex(d => d.name == item.name);
      this.A_database.object(this.pathList)
      .snapshotChanges().map(action => {
        const data = action.payload.toJSON();
        return data;
      }).takeUntil(this.ngUnsubscribe)
      .subscribe(result => {
        Object.keys(result).map(key => {
          returnedArray.push({ 'key': key, 'data':result[key]});
        });
        // Chargement d' une photo
        
        let Url: string = '/'+randomVal+'.jpeg';
        let imgUrl:String=""; 
        storage().ref(Url).getDownloadURL().then(function(url){
          imgUrl = url;
          return url ;
        }).then((url)=>{
           // Greffe de la photo dans l arbre de la liste selectionnée
            
          var UpdatedList = {
            name: item.name,
            uuid: this.authCtrl.auth.currentUser.uid,
            photo: url
          }
          this.A_database.list(this.pathList).update(returnedArray[ItemIndex].key,UpdatedList);
        }); 
      });
      
    }catch(err){
      console.log(err);
    }
  }

}
