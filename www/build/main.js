webpackJsonp([5],{

/***/ 204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InscriptionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(60);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var InscriptionPage = (function () {
    function InscriptionPage(navCtrl, navParams, authCtrl, fireDB) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.authCtrl = authCtrl;
        this.fireDB = fireDB;
        this.user = {};
    }
    InscriptionPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InscriptionPage');
    };
    InscriptionPage.prototype.subscribe = function (userprof) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var result, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.authCtrl.auth.createUserWithEmailAndPassword(userprof.email, userprof.password)];
                    case 1:
                        result = _a.sent();
                        console.log("Inscription Valide!");
                        if (result) {
                            this.authCtrl.auth.onAuthStateChanged(function (user) {
                                if (user) {
                                    var userbis = {
                                        firstName: userprof.firstName,
                                        lastName: userprof.lastName,
                                        birthday: userprof.birthday,
                                        sex: userprof.sex
                                    };
                                    _this.fireDB.list('/Users').set(user.uid, userbis);
                                    _this.fireDB.list('/Users/' + user.uid + '/Lists').push({
                                        uuid: "0",
                                        name: "Default"
                                    });
                                    _this.fireDB.list('/Users/' + user.uid + '/Items').push({
                                        lid: "0",
                                        name: "Default",
                                        desc: "",
                                        complete: "false"
                                    });
                                }
                            });
                        }
                        this.navCtrl.pop();
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        console.log("Erreur Inscription " + error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    InscriptionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-inscription',template:/*ion-inline-start:"/home/hacknblast/Bureau/Ab New School/FOURTH YEAR/S2/Développement Mobile/TPS/TodoList_v0/src/pages/inscription/inscription.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Creer un compte</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n  <ion-list>\n    <ion-item>\n      <ion-label color="primary" floating>Email</ion-label>\n      <ion-input type="text" [(ngModel)]="user.email"></ion-input>\n    </ion-item>\n  \n    <ion-item>\n      <ion-label color="primary" floating>Mot de passe</ion-label>\n      <ion-input type="password" [(ngModel)]="user.password"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label color="primary" floating>Prenom</ion-label>\n      <ion-input type="text" [(ngModel)]="user.firstName"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label color="primary" floating>Nom</ion-label>\n      <ion-input type="text" [(ngModel)]="user.lastName"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label color="primary" floating>Date de naissance</ion-label>\n      <ion-datetime displayFormat="MMM DD, YYYY" [(ngModel)]="user.birthday"></ion-datetime>\n    </ion-item>\n\n    <ion-item>\n      <ion-label color="primary" floating>Sexe</ion-label>\n      <ion-select [(ngModel)]="user.sex">\n        <ion-option value="Homme">Homme</ion-option>\n        <ion-option value="Femme">Femme</ion-option>\n      </ion-select>\n    </ion-item>\n\n    \n    <div class="row" >\n      <div class="col" ></div>\n      <div class="col" >\n        <button ion-button item-end color="secondary" (click)="subscribe(user)">\n          <ion-icon name="checkmark-circle"> Valider</ion-icon>\n        </button>\n      </div>\n      <div class="col" ></div>\n    </div>\n    \n\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/home/hacknblast/Bureau/Ab New School/FOURTH YEAR/S2/Développement Mobile/TPS/TodoList_v0/src/pages/inscription/inscription.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */]])
    ], InscriptionPage);
    return InscriptionPage;
}());

//# sourceMappingURL=inscription.js.map

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_takeUntil__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_takeUntil___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_takeUntil__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProfilPage = (function () {
    function ProfilPage(navCtrl, navParams, A_database) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.A_database = A_database;
        this.ngUnsubscribe = new __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__["Subject"]();
        this.profilDB = {
            firstName: "",
            lastName: "",
            birthday: "",
            sex: ""
        };
        console.log('Connected as ' + this.navParams.get('user').email);
    }
    ProfilPage.prototype.ngOnInit = function () {
        var _this = this;
        this.subscription = this.A_database.object('/Users/' + this.navParams.get('user').uid)
            .snapshotChanges().map(function (action) {
            var data = action.payload.toJSON();
            return data;
        }).takeUntil(this.ngUnsubscribe)
            .subscribe(function (result) {
            var Valretour = [];
            Object.keys(result).map(function (key) {
                Valretour.push({ 'key': key, 'data': result[key] });
            });
            _this.profilDB.birthday = Valretour[2].data;
            _this.profilDB.firstName = Valretour[3].data;
            _this.profilDB.lastName = Valretour[4].data;
            _this.profilDB.sex = Valretour[5].data;
        });
    };
    ProfilPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilPage');
    };
    ProfilPage.prototype.ngOnDestroy = function () {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    };
    ProfilPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profil',template:/*ion-inline-start:"/home/hacknblast/Bureau/Ab New School/FOURTH YEAR/S2/Développement Mobile/TPS/TodoList_v0/src/pages/profil/profil.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Profil</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div class="row" >\n    <div class="col" >  Prénom : </div>\n    <div class="col" >  {{ profilDB.firstName }} </div>\n  </div>\n  <div class="row" >\n    <div class="col" >  Nom : </div>\n    <div class="col" >  {{ profilDB.lastName }} </div>\n  </div>\n  <div class="row" >\n    <div class="col" >  Date de naissance : </div>\n    <div class="col" >  {{ profilDB.birthday }} </div>\n  </div>\n  <div class="row" >\n    <div class="col" >  Genre : </div>\n    <div class="col" >  {{ profilDB.sex }} </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/home/hacknblast/Bureau/Ab New School/FOURTH YEAR/S2/Développement Mobile/TPS/TodoList_v0/src/pages/profil/profil.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */]])
    ], ProfilPage);
    return ProfilPage;
}());

//# sourceMappingURL=profil.js.map

/***/ }),

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TodoItemsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__ = __webpack_require__(550);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_Subject__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_Subject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_takeUntil__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_takeUntil___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_takeUntil__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var TodoItemsPage = (function () {
    function TodoItemsPage(navCtrl, navParams, alertCtrl, modalCtrl, A_database) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.A_database = A_database;
        this.ngUnsubscribe = new __WEBPACK_IMPORTED_MODULE_5_rxjs_Subject__["Subject"]();
        this.items = [];
        this.title = this.navParams.get('selectedList').name;
        this.pathItems = 'Users/' + this.navParams.get('user').uid + '/Items/';
        //console.log("Path Items : "+this.pathItems);
    }
    TodoItemsPage.prototype.backToList = function () {
        this.navCtrl.pop();
    };
    TodoItemsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TodoItemsPage');
    };
    TodoItemsPage.prototype.ngOnDestroy = function () {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    };
    TodoItemsPage.prototype.ngOnInit = function () {
        var _this = this;
        this.subscription = this.A_database.object(this.pathItems)
            .snapshotChanges().map(function (action) {
            var data = action.payload.toJSON();
            return data;
        }).takeUntil(this.ngUnsubscribe)
            .subscribe(function (result) {
            var returnedArray = [];
            Object.keys(result).map(function (key) {
                returnedArray.push({ 'key': key, 'data': result[key] });
            });
            var dataStore = returnedArray.map(function (val) { return val.data; });
            var affined = [];
            dataStore.map(function (val) {
                if (val.lid == _this.navParams.get('key')) {
                    affined.push(val);
                }
            });
            if (affined.length > 0) {
                //console.log(affined);
                _this.getTodo(affined).takeUntil(_this.ngUnsubscribe).subscribe(function (result) { return _this.items = result; });
            }
        });
    };
    TodoItemsPage.prototype.addItem = function () {
        var _this = this;
        var modal = this.modalCtrl.create('TodoAddItemPage');
        modal.onDidDismiss(function (info) {
            if (info.TaskName != "") {
                var addedItem = {
                    lid: _this.navParams.get('key'),
                    name: info.TaskName,
                    desc: info.Description,
                    complete: false
                };
                _this.A_database.list(_this.pathItems).push(addedItem);
            }
        });
        modal.present();
    };
    TodoItemsPage.prototype.deleteItemConfirm = function (selectedItem) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Suppression',
            message: 'Etes vous sûr de vouloir supprimer cette tâche ?',
            buttons: [
                {
                    text: 'Non',
                    handler: function () {
                        console.log('Non clicked');
                    }
                },
                {
                    text: 'Oui',
                    handler: function () {
                        console.log('Oui clicked');
                        console.log("Log check value Item: " + JSON.stringify(selectedItem.name));
                        var deletedItemIndex = _this.items.findIndex(function (d) { return d.name == selectedItem.name; });
                        _this.subscription = _this.A_database.object(_this.pathItems)
                            .snapshotChanges().map(function (action) {
                            var data = action.payload.toJSON();
                            return data;
                        }).takeUntil(_this.ngUnsubscribe)
                            .subscribe(function (result) {
                            var returnedArray = [];
                            Object.keys(result).map(function (key) {
                                returnedArray.push({ 'key': key, 'data': result[key] });
                            });
                            var affined = [];
                            returnedArray.map(function (val) {
                                if (val.data.lid == _this.navParams.get('key')) {
                                    affined.push(val);
                                }
                            });
                            try {
                                if (affined.length > 0) {
                                    //console.log(affined);
                                    _this.A_database.list(_this.pathItems).remove(affined[deletedItemIndex].key);
                                }
                            }
                            catch (error) {
                                console.log('undefined error');
                            }
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    TodoItemsPage.prototype.finishTask = function (item) {
        var _this = this;
        if (item.complete != true) {
            var taskItemIndex = this.items.findIndex(function (d) { return d.name == item.name; });
            this.subscription = this.A_database.object(this.pathItems)
                .snapshotChanges().map(function (action) {
                var data = action.payload.toJSON();
                return data;
            }).takeUntil(this.ngUnsubscribe)
                .subscribe(function (result) {
                var returnedArray = [];
                Object.keys(result).map(function (key) {
                    returnedArray.push({ 'key': key, 'data': result[key] });
                });
                var affined = [];
                returnedArray.map(function (val) {
                    if (val.data.lid == _this.navParams.get('key')) {
                        affined.push(val);
                    }
                });
                try {
                    if (affined.length > 0) {
                        //console.log(affined);
                        affined[taskItemIndex].data.complete = true;
                        _this.A_database.list(_this.pathItems).update(affined[taskItemIndex].key, affined[taskItemIndex].data);
                    }
                }
                catch (error) {
                    console.log('error');
                }
            });
        }
        else {
            var alert_1 = this.alertCtrl.create({
                title: 'Tâche',
                message: 'Cette tâche a déja été terminée!',
                buttons: ['OK']
            });
            alert_1.present();
        }
    };
    TodoItemsPage.prototype.modifItem = function (item) {
        var _this = this;
        var modal = this.modalCtrl.create('TodoAddItemPage');
        modal.onDidDismiss(function (info) {
            if (info.TaskName != "") {
                var selectedItem_1 = {
                    lid: item.lid,
                    name: info.TaskName,
                    desc: info.Description,
                    complete: item.complete
                };
                var modItemIndex = _this.items.findIndex(function (d) { return d.name == item.name; });
                _this.subscription = _this.A_database.object(_this.pathItems)
                    .snapshotChanges().map(function (action) {
                    var data = action.payload.toJSON();
                    return data;
                }).takeUntil(_this.ngUnsubscribe)
                    .subscribe(function (result) {
                    var returnedArray = [];
                    Object.keys(result).map(function (key) {
                        returnedArray.push({ 'key': key, 'data': result[key] });
                    });
                    var affined = [];
                    returnedArray.map(function (val) {
                        if (val.data.lid == _this.navParams.get('key')) {
                            affined.push(val);
                        }
                    });
                    try {
                        if (affined.length > 0) {
                            //console.log(affined);
                            _this.A_database.list(_this.pathItems).update(affined[modItemIndex].key, selectedItem_1);
                        }
                    }
                    catch (error) {
                        console.log('error');
                    }
                });
            }
        });
        modal.present();
    };
    TodoItemsPage.prototype.getTodo = function (data) {
        return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].of(data);
    };
    TodoItemsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-todo-items',template:/*ion-inline-start:"/home/hacknblast/Bureau/Ab New School/FOURTH YEAR/S2/Développement Mobile/TPS/TodoList_v0/src/pages/todo-items/todo-items.html"*/'<ion-header>\n\n  <ion-navbar hideBackButton>\n    <ion-buttons start>\n        <button ion-button icon-only (click)="backToList()" color="Greycol">\n          <ion-icon name="arrow-dropleft-circle"></ion-icon>\n        </button>\n    </ion-buttons>\n    <ion-buttons end>\n          <button ion-button icon-only (click)="addItem()">\n            <ion-icon name="add"></ion-icon>\n          </button>\n    </ion-buttons>\n    <ion-title>{{title}}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n    <ion-list inset>\n        <ion-item-sliding *ngFor="let item of items">\n            <ion-item>\n                <ion-card>\n                    <ion-card-header>\n                        {{ item.name }}\n                    </ion-card-header>\n                    <ion-card-content>\n                        <b><p>State : {{ item.complete ===true ? \'Finished\' : \'Unfinished\' }}</p></b>\n                        <p>{{ item.desc }}</p>\n                    </ion-card-content>\n                </ion-card>\n            </ion-item>\n\n            <ion-item-options side="left">\n                <button ion-button icon-only item-end color="secondary" (click)="finishTask(item)">\n                    <ion-icon name="checkmark-circle"></ion-icon>\n                </button>\n            </ion-item-options>\n            <ion-item-options side="right">\n                    <button ion-button icon-only item-end color="primary" (click)="modifItem(item)">\n                          <ion-icon name="create"></ion-icon>\n                    </button>\n                    <button ion-button icon-only item-end color="danger" (click)="deleteItemConfirm(item)">\n                        <ion-icon name="trash"></ion-icon>\n                    </button>\n            </ion-item-options>\n            \n        </ion-item-sliding>  \n    </ion-list>\n</ion-content>'/*ion-inline-end:"/home/hacknblast/Bureau/Ab New School/FOURTH YEAR/S2/Développement Mobile/TPS/TodoList_v0/src/pages/todo-items/todo-items.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */]])
    ], TodoItemsPage);
    return TodoItemsPage;
}());

//# sourceMappingURL=todo-items.js.map

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TodoServicePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__todo_items_todo_items__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__profil_profil__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_firebase__ = __webpack_require__(809);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_Subject__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_Subject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_takeUntil__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_takeUntil___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_takeUntil__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};











var TodoServicePage = (function () {
    function TodoServicePage(navCtrl, navParams, alertCtrl, A_database, authCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.A_database = A_database;
        this.authCtrl = authCtrl;
        this.ngUnsubscribe = new __WEBPACK_IMPORTED_MODULE_8_rxjs_Subject__["Subject"]();
        this.lists = [];
        console.log("Current user UID : " + this.navParams.get('arg1').uid);
        this.pathList = 'Users/' + this.navParams.get('arg1').uid + '/Lists/';
        console.log("Path List : " + this.pathList);
    }
    TodoServicePage.prototype.ngOnInit = function () {
        var _this = this;
        this.subscription = this.A_database.object(this.pathList)
            .snapshotChanges().map(function (action) {
            var data = action.payload.toJSON();
            return data;
        }).takeUntil(this.ngUnsubscribe)
            .subscribe(function (result) {
            var returnedArray = [];
            Object.keys(result).map(function (key) {
                returnedArray.push({ 'key': key, 'data': result[key] });
            });
            var affined = returnedArray.map(function (val) { return val.data; });
            _this.getList(affined).takeUntil(_this.ngUnsubscribe).subscribe(function (lists) { return _this.lists = lists; });
            //console.log(this.lists);
        });
    };
    TodoServicePage.prototype.ngOnDestroy = function () {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    };
    TodoServicePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TodoServicePage');
    };
    TodoServicePage.prototype.listSelected = function (item) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var returnedArray, ItemIndex, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        returnedArray = [];
                        ItemIndex = this.lists.findIndex(function (d) { return d.name == item.name; });
                        _a = this;
                        return [4 /*yield*/, this.A_database.object(this.pathList)
                                .snapshotChanges().map(function (action) {
                                var data = action.payload.toJSON();
                                return data;
                            }).takeUntil(this.ngUnsubscribe)
                                .subscribe(function (result) {
                                Object.keys(result).map(function (key) {
                                    returnedArray.push({ 'key': key, 'data': result[key] });
                                });
                                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__todo_items_todo_items__["a" /* TodoItemsPage */], {
                                    selectedList: item,
                                    key: returnedArray[ItemIndex].key,
                                    user: _this.navParams.get('arg1')
                                });
                            })];
                    case 1:
                        _a.subscription = _b.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    TodoServicePage.prototype.logOut = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.authCtrl.auth.signOut().then(function (rej) {
                                _this.A_database.database.ref(_this.pathList).onDisconnect().cancel();
                                _this.pathList = "";
                                _this.navCtrl.pop();
                                console.log('Disconnected successfully!');
                            })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        console.log("Probleme de deconnexion");
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    TodoServicePage.prototype.addList = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Nouvelle Liste',
            message: "Veuillez saisir le nom de la liste à ajouter",
            inputs: [
                {
                    name: 'newList',
                    placeholder: 'Nom'
                },
            ],
            buttons: [
                {
                    text: 'Annuler',
                    handler: function (data) {
                        console.log('Annuler clicked');
                    }
                },
                {
                    text: 'Confirmer',
                    handler: function (data) {
                        _this.A_database.list(_this.pathList).push({
                            uuid: _this.navParams.get('arg1').uid,
                            name: data.newList
                        });
                    }
                }
            ]
        });
        prompt.present();
    };
    TodoServicePage.prototype.deleteListConfirm = function (selectedList) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Suppression',
            message: 'Etes vous sûr de vouloir supprimer cette liste ?',
            buttons: [
                {
                    text: 'Non',
                    handler: function () {
                        console.log('Non clicked');
                    }
                },
                {
                    text: 'Oui',
                    handler: function () {
                        console.log('Oui clicked');
                        console.log("Log check value Item: " + JSON.stringify(selectedList.name));
                        var returnedArray = [];
                        var deletedItemIndex = _this.lists.findIndex(function (d) { return d.name == selectedList.name; });
                        _this.subscription = _this.A_database.object(_this.pathList)
                            .snapshotChanges().map(function (action) {
                            var data = action.payload.toJSON();
                            return data;
                        }).takeUntil(_this.ngUnsubscribe)
                            .subscribe(function (result) {
                            Object.keys(result).map(function (key) {
                                returnedArray.push({ 'key': key, 'data': result[key] });
                            });
                            //console.log(returnedArray[deletedItemIndex].key);
                            _this.A_database.list(_this.pathList).remove(returnedArray[deletedItemIndex].key);
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    TodoServicePage.prototype.modifList = function (item) {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Renommer Liste',
            message: "Veuillez saisir le nouveau nom de la liste",
            inputs: [
                {
                    name: 'modList',
                    placeholder: 'Nom'
                },
            ],
            buttons: [
                {
                    text: 'Annuler',
                    handler: function (data) {
                        console.log('Annuler clicked');
                    }
                },
                {
                    text: 'Confirmer',
                    handler: function (data) {
                        var returnedArray = [];
                        var modItemIndex = _this.lists.findIndex(function (d) { return d.name == item.name; });
                        var UpdatedList = {
                            name: data.modList,
                            uuid: _this.lists[modItemIndex].uuid
                        };
                        _this.subscription = _this.A_database.object(_this.pathList)
                            .snapshotChanges().map(function (action) {
                            var data = action.payload.toJSON();
                            return data;
                        }).takeUntil(_this.ngUnsubscribe)
                            .subscribe(function (result) {
                            Object.keys(result).map(function (key) {
                                returnedArray.push({ 'key': key, 'data': result[key] });
                            });
                            _this.A_database.list(_this.pathList).update(returnedArray[modItemIndex].key, UpdatedList);
                        });
                    }
                }
            ]
        });
        prompt.present();
    };
    TodoServicePage.prototype.showProfil = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__profil_profil__["a" /* ProfilPage */], {
            user: this.navParams.get('arg1')
        });
    };
    TodoServicePage.prototype.getList = function (data) {
        return __WEBPACK_IMPORTED_MODULE_6_rxjs_Observable__["Observable"].of(data);
    };
    TodoServicePage.prototype.selectPhoto = function (item) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var randomVal_1, returnedArray_1, ItemIndex;
            return __generator(this, function (_a) {
                try {
                    randomVal_1 = Math.floor(Math.random() * 10) + 1;
                    returnedArray_1 = [];
                    ItemIndex = this.lists.findIndex(function (d) { return d.name == item.name; });
                    this.A_database.object(this.pathList)
                        .snapshotChanges().map(function (action) {
                        var data = action.payload.toJSON();
                        return data;
                    }).takeUntil(this.ngUnsubscribe)
                        .subscribe(function (result) {
                        Object.keys(result).map(function (key) {
                            returnedArray_1.push({ 'key': key, 'data': result[key] });
                        });
                        // Chargement d' une photo
                        var Url = '/' + randomVal_1 + '.jpeg';
                        var imgUrl = "";
                        Object(__WEBPACK_IMPORTED_MODULE_7_firebase__["storage"])().ref(Url).getDownloadURL().then(function (url) {
                            imgUrl = url;
                            return url;
                        }).then(function (url) {
                            // Greffe de la photo dans l arbre de la liste selectionnée
                            var UpdatedList = {
                                name: item.name,
                                uuid: _this.authCtrl.auth.currentUser.uid,
                                photo: url
                            };
                            _this.A_database.list(_this.pathList).update(returnedArray_1[ItemIndex].key, UpdatedList);
                        });
                    });
                }
                catch (err) {
                    console.log(err);
                }
                return [2 /*return*/];
            });
        });
    };
    TodoServicePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-todo-service',template:/*ion-inline-start:"/home/hacknblast/Bureau/Ab New School/FOURTH YEAR/S2/Développement Mobile/TPS/TodoList_v0/src/pages/todo-service/todo-service.html"*/'<ion-header>\n\n  <ion-navbar hideBackButton>\n      <ion-buttons end>\n        <button ion-button icon-only (click)="addList()">\n          <ion-icon name="add"></ion-icon>\n        </button>\n      </ion-buttons>\n      <ion-buttons end>\n          <button ion-button icon-only (click)="sharedList()">\n            <ion-icon name="people"></ion-icon>\n         </button>\n      </ion-buttons>  \n      <ion-buttons start>\n          <button ion-button icon-only (click)="logOut()" color="Greycol">\n            <ion-icon name="power"></ion-icon>\n          </button>\n      </ion-buttons>\n      <ion-buttons start>\n        <button ion-button icon-only (click)="showProfil()" color="Greycol">\n          <ion-icon name="contact"></ion-icon>\n        </button>\n    </ion-buttons>\n    <ion-title>Menu</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n    <ion-list inset>\n        <ion-item-sliding *ngFor="let item of lists">\n          <button ion-item  (click)="listSelected(item)">\n          <ion-avatar item-start>\n              <img src= "{{item.photo}}">\n          </ion-avatar> \n          {{ item.name }} <!--<b>({{ item.items.length }} {{ item.photo }})</b>-->\n          </button>\n          \n          <ion-item-options side="left">\n              <button ion-button icon-only item-end color="ClearBlue" (click)="shareList(item)">\n                  <ion-icon name="share"></ion-icon>\n              </button>\n          </ion-item-options>\n\n          <ion-item-options side="right">\n              <button ion-button icon-only item-end color="primary" (click)="modifList(item)">\n                  <ion-icon name="create"></ion-icon>\n              </button>\n              <button ion-button icon-only item-end color="secondary" (click)="selectPhoto(item)">\n                <ion-icon name="camera"></ion-icon>\n              </button>\n              <button ion-button icon-only item-end color="danger" (click)="deleteListConfirm(item)">\n                <ion-icon name="trash"></ion-icon>\n              </button>\n          </ion-item-options>\n\n    </ion-item-sliding>  \n  </ion-list>\n</ion-content>\n\n'/*ion-inline-end:"/home/hacknblast/Bureau/Ab New School/FOURTH YEAR/S2/Développement Mobile/TPS/TodoList_v0/src/pages/todo-service/todo-service.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["a" /* AngularFireAuth */]])
    ], TodoServicePage);
    return TodoServicePage;
}());

//# sourceMappingURL=todo-service.js.map

/***/ }),

/***/ 220:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 220;

/***/ }),

/***/ 265:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/inscription/inscription.module": [
		865,
		4
	],
	"../pages/profil/profil.module": [
		866,
		3
	],
	"../pages/todo-add-item/todo-add-item.module": [
		867,
		0
	],
	"../pages/todo-items/todo-items.module": [
		868,
		2
	],
	"../pages/todo-service/todo-service.module": [
		869,
		1
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 265;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 456:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__todo_service_todo_service__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__inscription_inscription__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_plus__ = __webpack_require__(457);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_database__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_firebase_app__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_firebase_app___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_firebase_app__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_Subject__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_Subject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_takeUntil__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_takeUntil___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_takeUntil__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};











var HomePage = (function () {
    function HomePage(navCtrl, authCtrl, fireDB, msg, google, platf) {
        this.navCtrl = navCtrl;
        this.authCtrl = authCtrl;
        this.fireDB = fireDB;
        this.msg = msg;
        this.google = google;
        this.platf = platf;
        this.ngUnsubscribe = new __WEBPACK_IMPORTED_MODULE_8_rxjs_Subject__["Subject"]();
        this.user = this.authCtrl.authState;
    }
    HomePage.prototype.showTodoPage = function (login, mdp) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var result, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.authCtrl.auth.signInWithEmailAndPassword(login, mdp)];
                    case 1:
                        result = _a.sent();
                        if (result) {
                            this.authCtrl.auth.onAuthStateChanged(function (user) {
                                if (user) {
                                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__todo_service_todo_service__["a" /* TodoServicePage */], {
                                        arg1: user
                                    });
                                }
                            });
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        this.msg.create({
                            message: error_1,
                            duration: 5000
                        }).present();
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.createUser = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__inscription_inscription__["a" /* InscriptionPage */]);
    };
    HomePage.prototype.loginGoogle = function () {
        /*if(this.platf.is('cordova')){
          this.nativeLogin();
        }else{*/
        this.webLogin();
        // }  
    };
    HomePage.prototype.nativeLogin = function () {
        return __awaiter(this, void 0, void 0, function () {
            var googleUser, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        return [4 /*yield*/, this.google.login({
                                'webClientId': '229004801619-dc3bdboe3lg9j59k1i934u31qo4fac16.apps.googleusercontent.com',
                                'offline': true,
                                'scopes': 'profile email'
                            })];
                    case 1:
                        googleUser = _a.sent();
                        return [4 /*yield*/, this.authCtrl.auth.signInWithCredential(
                            // Vrai -> //firebase.auth.GoogleAuthProvider.credential(googleUser.idToken)
                            googleUser.idToken)];
                    case 2: return [2 /*return*/, _a.sent()];
                    case 3:
                        error_2 = _a.sent();
                        console.log(error_2);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.webLogin = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var provider, credential, error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        provider = new __WEBPACK_IMPORTED_MODULE_7_firebase_app__["auth"].GoogleAuthProvider();
                        provider.setCustomParameters({
                            prompt: 'select_account'
                        });
                        return [4 /*yield*/, this.authCtrl.auth.signInWithPopup(provider)];
                    case 1:
                        credential = _a.sent();
                        if (credential) {
                            this.authCtrl.auth.onAuthStateChanged(function (user) {
                                if (user) {
                                    _this.subscription = _this.fireDB.object('/Users/')
                                        .snapshotChanges().map(function (action) {
                                        var data = action.payload.toJSON();
                                        return data;
                                    }).takeUntil(_this.ngUnsubscribe)
                                        .subscribe(function (result) {
                                        var returnedArray = [];
                                        Object.keys(result).map(function (key) {
                                            returnedArray.push({ 'key': key, 'data': result[key] });
                                        });
                                        var affined = returnedArray.map(function (val) { return val.key; });
                                        var check = false;
                                        while (affined.length > 0) {
                                            var current_id = affined.pop();
                                            if (current_id === user.uid) {
                                                check = true;
                                            }
                                        }
                                        console.log("Utilisateur existant ? : " + check);
                                        if (check) {
                                            _this.ngOnDestroy();
                                            console.log(" Compte deja existant -> Connexion normale");
                                            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__todo_service_todo_service__["a" /* TodoServicePage */], {
                                                arg1: user
                                            });
                                        }
                                        else {
                                            console.log(" Nouveau compte -> Inscription + connexion");
                                            var userbis = {
                                                firstName: user.displayName,
                                                lastName: "Unknown",
                                                birthday: "Unknown",
                                                sex: "Unknown"
                                            };
                                            _this.fireDB.list('/Users').set(user.uid, userbis);
                                            _this.fireDB.list('/Users/' + user.uid + '/Lists').push({
                                                uuid: "0",
                                                name: "Default"
                                            });
                                            _this.fireDB.list('/Users/' + user.uid + '/Items').push({
                                                lid: "0",
                                                name: "Default",
                                                desc: "",
                                                complete: "false"
                                            });
                                            _this.ngOnDestroy();
                                            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__todo_service_todo_service__["a" /* TodoServicePage */], {
                                                arg1: user
                                            });
                                        }
                                    });
                                }
                            });
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        error_3 = _a.sent();
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.ngOnInit = function () {
    };
    HomePage.prototype.ngOnDestroy = function () {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    };
    HomePage.prototype.logoutGoogle = function () {
        this.authCtrl.auth.signOut();
        /*if(this.platf.is('cordova')){
          this.google.logout();
        }*/
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/home/hacknblast/Bureau/Ab New School/FOURTH YEAR/S2/Développement Mobile/TPS/TodoList_v0/src/pages/home/home.html"*/'\n\n<ion-content class="background">\n  <ion-card>\n      <ion-card-header>\n          <b>My Todo-App</b>\n      </ion-card-header> \n      <ion-card-content>\n          <ion-list no-line>\n            <ion-item>\n              <ion-input type = "text" placeholder="Login" [(ngModel)]="login"></ion-input>\n            </ion-item>\n\n            <ion-item>\n              <ion-input type = "password" placeholder="Mot de Passe" [(ngModel)]="mdp"></ion-input>\n            </ion-item>\n          </ion-list>\n          <a>Mot de passe oublié ?</a>\n          <div class="row" >\n              <div class="col" ></div>\n              <div class="col" >\n                  <button ion-button round color="dark" (click)="showTodoPage(login,mdp)">Connexion</button>\n              </div>\n              <div class="col" ></div>\n          </div>\n          \n          <button ion-button full color color="Google" (click)="loginGoogle()">\n            <ion-icon name="logo-google"></ion-icon>Connexion via Google\n          </button>\n          <button ion-button full color="Facebook">\n            <ion-icon name="logo-facebook"></ion-icon>Connexion via Facebook\n          </button>\n      </ion-card-content> \n  </ion-card>\n  <div class="row" >\n      <div class="col" ></div>\n      <div class="col" >\n          <button ion-button round color="dark" (click)="createUser()">Creer un nouveau compte</button>\n      </div>\n      <div class="col" ></div>\n     \n  </div>\n</ion-content>\n'/*ion-inline-end:"/home/hacknblast/Bureau/Ab New School/FOURTH YEAR/S2/Développement Mobile/TPS/TodoList_v0/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_6_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_plus__["a" /* GooglePlus */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 458:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(459);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(463);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 463:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(453);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(455);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angularfire2_database__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_firebase_config__ = __webpack_require__(863);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_component__ = __webpack_require__(864);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_home_home__ = __webpack_require__(456);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_todo_service_todo_service__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_todo_items_todo_items__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_inscription_inscription__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_profil_profil__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_google_plus__ = __webpack_require__(457);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_9__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_10__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_todo_service_todo_service__["a" /* TodoServicePage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_todo_items_todo_items__["a" /* TodoItemsPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_inscription_inscription__["a" /* InscriptionPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_profil_profil__["a" /* ProfilPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_9__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/inscription/inscription.module#InscriptionPageModule', name: 'InscriptionPage', segment: 'inscription', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profil/profil.module#ProfilPageModule', name: 'ProfilPage', segment: 'profil', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/todo-add-item/todo-add-item.module#TodoAddItemPageModule', name: 'TodoAddItemPage', segment: 'todo-add-item', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/todo-items/todo-items.module#TodoItemsPageModule', name: 'TodoItemsPage', segment: 'todo-items', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/todo-service/todo-service.module#TodoServicePageModule', name: 'TodoServicePage', segment: 'todo-service', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_5_angularfire2__["a" /* AngularFireModule */].initializeApp(__WEBPACK_IMPORTED_MODULE_8__app_firebase_config__["a" /* FIREBASE_CONFIG */]),
                __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__["b" /* AngularFireAuthModule */],
                __WEBPACK_IMPORTED_MODULE_7_angularfire2_database__["b" /* AngularFireDatabaseModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_9__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_10__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_todo_service_todo_service__["a" /* TodoServicePage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_todo_items_todo_items__["a" /* TodoItemsPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_inscription_inscription__["a" /* InscriptionPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_profil_profil__["a" /* ProfilPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_google_plus__["a" /* GooglePlus */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 863:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FIREBASE_CONFIG; });
var FIREBASE_CONFIG = {
    apiKey: "AIzaSyCHt72RZz4lJulOH-56Cv8AQkHxB9xyHVg",
    authDomain: "todolist-m2pgi-a4a1b.firebaseapp.com",
    databaseURL: "https://todolist-m2pgi-a4a1b.firebaseio.com",
    projectId: "todolist-m2pgi-a4a1b",
    storageBucket: "todolist-m2pgi-a4a1b.appspot.com",
    messagingSenderId: "229004801619"
};
//# sourceMappingURL=app.firebase.config.js.map

/***/ }),

/***/ 864:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(455);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(453);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(456);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/hacknblast/Bureau/Ab New School/FOURTH YEAR/S2/Développement Mobile/TPS/TodoList_v0/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/home/hacknblast/Bureau/Ab New School/FOURTH YEAR/S2/Développement Mobile/TPS/TodoList_v0/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ })

},[458]);
//# sourceMappingURL=main.js.map